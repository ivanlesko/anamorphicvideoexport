//
//  Constants.m
//  AVCam
//
//  Created by Ivan Lesko on 5/8/14.
//  Copyright (c) 2014 Apple Inc. All rights reserved.
//

#import "Constants.h"

@implementation Constants

NSString *const kAVIsExporting = @"kAVIsExporting";

@end
