/*
     File: AVCamViewController.m
 Abstract: View controller for camera interface.
  Version: 3.1
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2014 Apple Inc. All Rights Reserved.
 
 */

#import "AVCamViewController.h"

#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>

#import "AVCamPreviewView.h"

#import "NSString+Directories.h"

#import "ProgressView.h"

static void * CapturingStillImageContext = &CapturingStillImageContext;
static void * RecordingContext = &RecordingContext;
static void * SessionRunningAndDeviceAuthorizedContext = &SessionRunningAndDeviceAuthorizedContext;

@interface AVCamViewController () <AVCaptureFileOutputRecordingDelegate>

// For use in the storyboards.
@property (nonatomic, weak) IBOutlet AVCamPreviewView *previewView;
@property (nonatomic, weak) IBOutlet UIButton *recordButton;
@property (nonatomic, weak) IBOutlet UIButton *cameraButton;
@property (nonatomic, weak) IBOutlet UIButton *stillButton;

- (IBAction)toggleMovieRecording:(id)sender;
- (IBAction)focusAndExposeTap:(UIGestureRecognizer *)gestureRecognizer;

// Session management.
@property (nonatomic) dispatch_queue_t sessionQueue; // Communicate with the session and other session objects on this queue.
@property (nonatomic) AVCaptureSession *session;
@property (nonatomic) AVCaptureDeviceInput *videoDeviceInput;
@property (nonatomic) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic) AVCaptureMetadataOutput *metadataOutput;
@property (nonatomic) CMSampleBufferRef sampleBuffer;
@property (nonatomic) AVAssetExportSession *exportSession;

@property (nonatomic) NSString *dateStampString;

// Utilities.
@property (nonatomic) UIBackgroundTaskIdentifier backgroundRecordingID;
@property (nonatomic, getter = isDeviceAuthorized) BOOL deviceAuthorized;
@property (nonatomic, readonly, getter = isSessionRunningAndDeviceAuthorized) BOOL sessionRunningAndDeviceAuthorized;
@property (nonatomic) BOOL lockInterfaceRotation;
@property (nonatomic) id runtimeErrorHandlingObserver;

@property (nonatomic) MPMoviePlayerController *playercontroller;

// Export Progress Views & Utilities
@property (weak, nonatomic) IBOutlet ProgressView *progressView;
@property (nonatomic, strong) NSTimer *progressTimer;

@end

@implementation AVCamViewController

@synthesize videoFrameRate, videoDimensions, videoType;

- (BOOL)isSessionRunningAndDeviceAuthorized
{
	return [[self session] isRunning] && [self isDeviceAuthorized];
}

+ (NSSet *)keyPathsForValuesAffectingSessionRunningAndDeviceAuthorized
{
	return [NSSet setWithObjects:@"session.running", @"deviceAuthorized", nil];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    self.recordButton.layer.cornerRadius = self.recordButton.bounds.size.width / 2.0;
    
    // Clear the documents directory on application launch to have a clean directory to work with.
    [self clearDocumentsDirectory];
	
	// Create the AVCaptureSession
	AVCaptureSession *session = [[AVCaptureSession alloc] init];
	[self setSession:session];
	
	// Setup the preview view
	[[self previewView] setSession:session];
	
	// Check for device authorization
	[self checkDeviceAuthorizationStatus];
	
	// In general it is not safe to mutate an AVCaptureSession or any of its inputs, outputs, or connections from multiple threads at the same time.
	// Why not do all of this on the main queue?
	// -[AVCaptureSession startRunning] is a blocking call which can take a long time. We dispatch session setup to the sessionQueue so that the main queue isn't blocked (which keeps the UI responsive).
	
	dispatch_queue_t sessionQueue = dispatch_queue_create("session queue", DISPATCH_QUEUE_SERIAL);
	[self setSessionQueue:sessionQueue];
	
	dispatch_async(sessionQueue, ^{
		[self setBackgroundRecordingID:UIBackgroundTaskInvalid];
		
		NSError *error = nil;
		
		AVCaptureDevice *videoDevice = [AVCamViewController deviceWithMediaType:AVMediaTypeVideo preferringPosition:AVCaptureDevicePositionBack];
		AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
        
        if (error)
        {
            NSLog(@"%@", error);
        }
        
        if ([session canAddInput:videoDeviceInput])
        {
            [session addInput:videoDeviceInput];
            [self setVideoDeviceInput:videoDeviceInput];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Why are we dispatching this to the main queue?
                // Because AVCaptureVideoPreviewLayer is the backing layer for AVCamPreviewView and UIView can only be manipulated on main thread.
                // Note: As an exception to the above rule, it is not necessary to serialize video orientation changes on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
                
                [[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] setVideoOrientation:(AVCaptureVideoOrientation)[self interfaceOrientation]];
            });
        }
        
        // Create, configure, and add the capture data output to the session
        AVCaptureVideoDataOutput *videoOut = [[AVCaptureVideoDataOutput alloc] init];
        
        /*
         AVCam prefers to discard late video frames early in the capture pipeline, since its
         processing can take longer than real-time on some platforms (such as iPhone 3GS).
         Clients whose image processing is faster than real-time should consider setting AVCaptureVideoDataOutput's
         alwaysDiscardsLateVideoFrames property to NO.
         */
        [videoOut setAlwaysDiscardsLateVideoFrames:YES];
        [videoOut setVideoSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
        dispatch_queue_t videoCaptureQueue = dispatch_queue_create("Video Capture Queue", DISPATCH_QUEUE_SERIAL);
        [videoOut setSampleBufferDelegate:self queue:videoCaptureQueue];
        if ([session canAddOutput:videoOut]) {
            [session addOutput:videoOut];
        
            videoConnection = [videoOut connectionWithMediaType:AVMediaTypeVideo];
            videoOrientation = [videoConnection videoOrientation];
		}
		
		AVCaptureDevice *audioDevice = [[AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio] firstObject];
		AVCaptureDeviceInput *audioDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:&error];
		
		if (error)
		{
			NSLog(@"%@", error);
		}
		
		if ([session canAddInput:audioDeviceInput])
		{
			[session addInput:audioDeviceInput];
		}
		
		AVCaptureMovieFileOutput *movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
		if ([session canAddOutput:movieFileOutput])
		{
			[session addOutput:movieFileOutput];
			AVCaptureConnection *connection = [movieFileOutput connectionWithMediaType:AVMediaTypeVideo];
			if ([connection isVideoStabilizationSupported])
				[connection setEnablesVideoStabilizationWhenAvailable:YES];
			[self setMovieFileOutput:movieFileOutput];
		}
		
		AVCaptureStillImageOutput *stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
		if ([session canAddOutput:stillImageOutput])
		{
			[stillImageOutput setOutputSettings:@{AVVideoCodecKey : AVVideoCodecJPEG}];
			[session addOutput:stillImageOutput];
			[self setStillImageOutput:stillImageOutput];
		}
	});
    
    [self.progressView hideExportViews];
}

- (void)viewWillAppear:(BOOL)animated
{
	dispatch_async([self sessionQueue], ^{
		[self addObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:SessionRunningAndDeviceAuthorizedContext];
		[self addObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:CapturingStillImageContext];
		[self addObserver:self forKeyPath:@"movieFileOutput.recording" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:RecordingContext];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[self videoDeviceInput] device]];
		
		__weak AVCamViewController *weakSelf = self;
		[self setRuntimeErrorHandlingObserver:[[NSNotificationCenter defaultCenter] addObserverForName:AVCaptureSessionRuntimeErrorNotification object:[self session] queue:nil usingBlock:^(NSNotification *note) {
			AVCamViewController *strongSelf = weakSelf;
			dispatch_async([strongSelf sessionQueue], ^{
				// Manually restarting the session since it must have been stopped due to an error.
				[[strongSelf session] startRunning];
				[[strongSelf recordButton] setTitle:NSLocalizedString(@"Record", @"Recording button record title") forState:UIControlStateNormal];
			});
		}]];
		[[self session] startRunning];
	});
}

- (void)viewDidDisappear:(BOOL)animated
{
	dispatch_async([self sessionQueue], ^{
		[[self session] stopRunning];
		
		[[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[self videoDeviceInput] device]];
		[[NSNotificationCenter defaultCenter] removeObserver:[self runtimeErrorHandlingObserver]];
		
		[self removeObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" context:SessionRunningAndDeviceAuthorizedContext];
		[self removeObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" context:CapturingStillImageContext];
		[self removeObserver:self forKeyPath:@"movieFileOutput.recording" context:RecordingContext];
	});
}

- (BOOL)prefersStatusBarHidden
{
	return YES;
}

- (BOOL)shouldAutorotate
{
	// Disable autorotation of the interface when recording is in progress.
	return ![self lockInterfaceRotation];
}

- (NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	[[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] setVideoOrientation:(AVCaptureVideoOrientation)toInterfaceOrientation];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (context == CapturingStillImageContext)
	{
		BOOL isCapturingStillImage = [change[NSKeyValueChangeNewKey] boolValue];
		
		if (isCapturingStillImage)
		{
			[self runStillImageCaptureAnimation];
		}
	}
	else if (context == RecordingContext)
	{
		BOOL isRecording = [change[NSKeyValueChangeNewKey] boolValue];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			if (isRecording)
			{
				[[self recordButton] setTitle:NSLocalizedString(@"stop", @"Recording button stop title") forState:UIControlStateNormal];
				[[self recordButton] setEnabled:YES];
			}
			else
			{
				[[self recordButton] setTitle:NSLocalizedString(@"rec", @"Recording button record title") forState:UIControlStateNormal];
				[[self recordButton] setEnabled:YES];
			}
		});
	}
	else if (context == SessionRunningAndDeviceAuthorizedContext)
	{
		BOOL isRunning = [change[NSKeyValueChangeNewKey] boolValue];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			if (isRunning)
			{
				[[self recordButton] setEnabled:YES];
			}
			else
			{
				[[self recordButton] setEnabled:NO];
			}
		});
	}
	else
	{
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}

#pragma mark Actions

- (IBAction)toggleMovieRecording:(id)sender
{
	[[self recordButton] setEnabled:NO];
	
	dispatch_async([self sessionQueue], ^{
		if (![[self movieFileOutput] isRecording])
		{
			[self setLockInterfaceRotation:YES];
			
			if ([[UIDevice currentDevice] isMultitaskingSupported])
			{
				// Setup background task. This is needed because the captureOutput:didFinishRecordingToOutputFileAtURL: callback is not received until AVCam returns to the foreground unless you request background execution time. This also ensures that there will be time to write the file to the assets library when AVCam is backgrounded. To conclude this background execution, -endBackgroundTask is called in -recorder:recordingDidFinishToOutputFileURL:error: after the recorded file has been saved.
				[self setBackgroundRecordingID:[[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil]];
			}
			
			// Update the orientation on the movie file output video connection before starting recording.
			[[[self movieFileOutput] connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation:[[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] videoOrientation]];
			
			// Turning OFF flash for video recording
			[AVCamViewController setFlashMode:AVCaptureFlashModeOff forDevice:[[self videoDeviceInput] device]];
			
			// Start recording to a temporary file.
			NSString *outputFilePath = [NSTemporaryDirectory() stringByAppendingString:[[NSString fileOutputNameForCurrentTime] stringByAppendingPathExtension:@"mov"]];
			[[self movieFileOutput] startRecordingToOutputFileURL:[NSURL fileURLWithPath:outputFilePath] recordingDelegate:self];
            self.dateStampString = [NSString fileOutputNameForCurrentTime];
            
            // Fade to red when the camera is recording.
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.5
                                 animations:^{
                                         self.recordButton.backgroundColor = [UIColor colorWithRed:.7
                                                                                             green:.05
                                                                                              blue:.05
                                                                                             alpha:.5];
                                 }];
            });
		}
		else
		{
			[[self movieFileOutput] stopRecording];
		}
	});
    
    // Fade to black when the recording has finished.
    if ([[self movieFileOutput] isRecording]) {
        if (self.progressView.processActivityIndicator.alpha == 0.0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.5
                                 animations:^{
                                     [self.progressView showExportViews];
                                     self.recordButton.alpha = 0.0f;
                                     self.recordButton.backgroundColor = [UIColor colorWithRed:0
                                                                                         green:0
                                                                                          blue:0
                                                                                         alpha:0.5];
                                 }];
            });
            
            
            [self.progressView.processActivityIndicator startAnimating];
        }
    }
}

- (IBAction)focusAndExposeTap:(UIGestureRecognizer *)gestureRecognizer
{
	CGPoint devicePoint = [(AVCaptureVideoPreviewLayer *)[[self previewView] layer] captureDevicePointOfInterestForPoint:[gestureRecognizer locationInView:[gestureRecognizer view]]];
	[self focusWithMode:AVCaptureFocusModeAutoFocus exposeWithMode:AVCaptureExposureModeAutoExpose atDevicePoint:devicePoint monitorSubjectAreaChange:YES];
}

- (void)subjectAreaDidChange:(NSNotification *)notification
{
	CGPoint devicePoint = CGPointMake(.5, .5);
	[self focusWithMode:AVCaptureFocusModeContinuousAutoFocus exposeWithMode:AVCaptureExposureModeContinuousAutoExposure atDevicePoint:devicePoint monitorSubjectAreaChange:NO];
}

#pragma mark File Output Delegate

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
	if (error)
		NSLog(@"%@", error);
	
	[self setLockInterfaceRotation:NO];
	
	// Note the backgroundRecordingID for use in the ALAssetsLibrary completion handler to end the background task associated with this recording. This allows a new recording to be started, associated with a new UIBackgroundTaskIdentifier, once the movie file output's -isRecording is back to NO — which happens sometime after this method returns.
	UIBackgroundTaskIdentifier backgroundRecordingID = [self backgroundRecordingID];
	[self setBackgroundRecordingID:UIBackgroundTaskInvalid];
	
	[[[ALAssetsLibrary alloc] init] writeVideoAtPathToSavedPhotosAlbum:outputFileURL completionBlock:^(NSURL *assetURL, NSError *error) {
		if (error) {
			NSLog(@"%@", error);
		} else {
            // Remove the temporary file created, since we are saving the final file to the photo library.
            [[NSFileManager defaultManager] removeItemAtURL:outputFileURL error:nil];
            NSLog(@"asset URL : %@", assetURL);
            
            if (backgroundRecordingID != UIBackgroundTaskInvalid) {
                [[UIApplication sharedApplication] endBackgroundTask:backgroundRecordingID];
            }
            
            AVAsset *videoAsset = [[AVURLAsset alloc] initWithURL:assetURL options:nil]; // The input
            
            [self exportVideoWithAsset:videoAsset withAssetURL:assetURL withAspectRatio:@(2.4)];
        }
	}];
}

- (void)exportVideoWithAsset:(AVAsset *)asset withAssetURL:(NSURL *)assetURL withAspectRatio:(NSNumber *)aspectWidthRatio {
    AVAssetTrack *clipVideoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] firstObject];
    
    AVMutableVideoComposition* mutableVideoComposition = [AVMutableVideoComposition videoComposition];
    mutableVideoComposition.renderSize = CGSizeMake([[NSNumber numberWithFloat:clipVideoTrack.naturalSize.height * aspectWidthRatio.floatValue] floatValue],
                                                    clipVideoTrack.naturalSize.height);
    
    CGSize originalSize = clipVideoTrack.naturalSize;
    CGSize newSize      = mutableVideoComposition.renderSize;
    
    [self updateProgressLabelsWithOriginalVideoSize:newSize andNewVideoSize:originalSize];
    
    NSLog(@"incoming clip dimensions: %@", NSStringFromCGSize(originalSize));
    NSLog(@"incoming clip length: %f seconds", CMTimeGetSeconds(asset.duration));
    NSLog(@"composition export dimensions: %@", NSStringFromCGSize(newSize));
    
    /*
     * Sets the frame rate of the video composition
     * May need to set the frame rate based on the incoming video.
     */
    mutableVideoComposition.frameDuration = CMTimeMake(1, 30);
    
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(60, 30));
    
    // Create the horizontal "De-squeeze" scale transform.
    AVMutableVideoCompositionLayerInstruction *transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];
    CGAffineTransform finalTransform = CGAffineTransformMakeScale([[NSNumber numberWithFloat:mutableVideoComposition.renderSize.width / clipVideoTrack.naturalSize.width] floatValue], 1.0f);
    
    [transformer setTransform:finalTransform atTime:kCMTimeZero];
    instruction.layerInstructions = @[transformer];
    mutableVideoComposition.instructions = @[instruction];
    
    self.exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
    self.exportSession.videoComposition = mutableVideoComposition;
    
    NSURL *outputURL = [NSURL fileURLWithPath:[[NSString documentsDirectory]
                                               stringByAppendingPathComponent:[[NSString stringWithFormat:@"%@_desqueezed", self.dateStampString]
                                                                               stringByAppendingPathExtension:@"mov"]]];
    self.exportSession.outputURL = outputURL;
    self.exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    
    NSLog(@"outputURL: %@", outputURL);
    
    [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
        switch (self.exportSession.status) {
            
            //==================//
            // Export Completed //
            //==================//
            case AVAssetExportSessionStatusCompleted:{ 
                NSLog(@"AVAssetExportSessionStatusCompleted");
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self exportDidFinish:self.exportSession withOutputURL:outputURL];
                    
                    // When the video finished exporting, it writes the output to the camera roll.
                    [[[ALAssetsLibrary alloc] init] writeVideoAtPathToSavedPhotosAlbum:outputURL completionBlock:^(NSURL *assetURL, NSError *error) {
                        if (error) {
                            NSLog(@"Desqueeze export to camera failed with error: %@", error.localizedDescription);
                        }
                    }];
                });
            }
                
            //==================//
            // Export Failed    //
            //==================//
            case AVAssetExportSessionStatusFailed: {
                NSLog(@"AVAssetExportSessionStatusFailed");
                NSLog(@"error: %@", self.exportSession.error);
                [self hideExportViewsAndShowRecordButton];
                
                // Alert the user that the export failed.  Allow them to try and re-export the last video saved.
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Export Error"
                                                                    message:@"Your video was not exported because of an interruption error."
                                                                   delegate:self
                                                          cancelButtonTitle:@"Cancel"
                                                          otherButtonTitles:nil,
                                                                           nil];
                    [alert show];
                });
                break;
            }
                
            case AVAssetExportSessionStatusWaiting: {
                NSLog(@"AVAssetExportSessionStatusWaiting");
                break;
            }
        }
    }];
    
    self.progressTimer = [NSTimer scheduledTimerWithTimeInterval:0.05
                                                          target:self
                                                        selector:@selector(updateExportDisplayWithExportSession:)
                                                        userInfo:nil
                                                         repeats:YES];
    
    [self updateExportDisplayWithExportSession:self.progressTimer];
}

- (void)updateProgressLabelsWithOriginalVideoSize:(CGSize)originalSize andNewVideoSize:(CGSize)newSize {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.progressView.exportOriginalVideoSizeLabel.text = [NSString stringWithFormat:@"original video size: %@", NSStringFromCGSize(originalSize)];
        self.progressView.exportNewVideoSizeLabel.text      = [NSString stringWithFormat:@"new video size: %@", NSStringFromCGSize(newSize)];
    });
}

- (void)updateExportDisplayWithExportSession:(NSTimer *)timer {
    if (self.exportSession.progress > .99) {
        [timer invalidate];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.exportSession.progress > 0 && self.exportSession.progress < .50) {
                self.progressView.exportProgressLabel.text = [NSString stringWithFormat:@"Processing video... %.0f/100", self.exportSession.progress * 100];
            }
            
            if (self.exportSession.progress >= .50 && self.exportSession.progress <= .74) {
                self.progressView.exportProgressLabel.text = [NSString stringWithFormat:@"Saving video to documents... %.0f/100", self.exportSession.progress * 100];
            }
            
            if (self.exportSession.progress >= .75 && self.exportSession.progress < .90) {
                self.progressView.exportProgressLabel.text = [NSString stringWithFormat:@"Saving video to camera roll... %.0f/100", self.exportSession.progress * 100];
            }
            
            if (self.exportSession.progress >= .90) {
                self.progressView.exportProgressLabel.text = [NSString stringWithFormat:@"Finalizing export... %.0f/100", self.exportSession.progress * 100];
            }
            
            self.progressView.exportProgressBar.progress = self.exportSession.progress;
        });
    }
}

- (void)exportDidFinish:(AVAssetExportSession *)exporter withOutputURL:(NSURL *)outputURL {
    // Configure the movie player when the export finishes.
    self.playercontroller = [[MPMoviePlayerController alloc] initWithContentURL:outputURL];
    self.playercontroller.view.frame = self.view.bounds;
    self.playercontroller.movieSourceType = MPMovieSourceTypeFile;
    self.playercontroller.scalingMode = MPMovieScalingModeAspectFit;
    self.playercontroller.fullscreen = YES;
    self.playercontroller.controlStyle = MPMovieControlStyleFullscreen;
    self.playercontroller.view.userInteractionEnabled = YES;
    self.playercontroller.view.backgroundColor = [UIColor blackColor];
    self.playercontroller.shouldAutoplay = NO;
    self.playercontroller.contentURL = outputURL;
    self.playercontroller.view.alpha = 0.0f;
    [self.playercontroller prepareToPlay];
    [self.view addSubview:self.playercontroller.view];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.playercontroller.view.alpha = 1.0f;
                     } completion:^(BOOL finished) {
                         [self hideExportViewsAndShowRecordButton];
                     }];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.progressView.exportProgressLabel.text = [NSString stringWithFormat:@"Processing video..."];
        self.progressView.exportOriginalVideoSizeLabel.text = @"calculating original size...";
        self.progressView.exportNewVideoSizeLabel.text      = @"calculating new video size...";
    });
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlaybackComplete:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.playercontroller];
}

#pragma mark Device Configuration

- (void)focusWithMode:(AVCaptureFocusMode)focusMode exposeWithMode:(AVCaptureExposureMode)exposureMode atDevicePoint:(CGPoint)point monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange
{
	dispatch_async([self sessionQueue], ^{
		AVCaptureDevice *device = [[self videoDeviceInput] device];
		NSError *error = nil;
		if ([device lockForConfiguration:&error])
		{
			if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:focusMode])
			{
				[device setFocusMode:focusMode];
				[device setFocusPointOfInterest:point];
			}
			if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:exposureMode])
			{
				[device setExposureMode:exposureMode];
				[device setExposurePointOfInterest:point];
			}
			[device setSubjectAreaChangeMonitoringEnabled:monitorSubjectAreaChange];
			[device unlockForConfiguration];
		}
		else
		{
			NSLog(@"%@", error);
		}
	});
}

+ (void)setFlashMode:(AVCaptureFlashMode)flashMode forDevice:(AVCaptureDevice *)device
{
	if ([device hasFlash] && [device isFlashModeSupported:flashMode])
	{
		NSError *error = nil;
		if ([device lockForConfiguration:&error])
		{
			[device setFlashMode:flashMode];
			[device unlockForConfiguration];
		}
		else
		{
			NSLog(@"%@", error);
		}
	}
}

+ (AVCaptureDevice *)deviceWithMediaType:(NSString *)mediaType preferringPosition:(AVCaptureDevicePosition)position
{
	NSArray *devices = [AVCaptureDevice devicesWithMediaType:mediaType];
	AVCaptureDevice *captureDevice = [devices firstObject];
	
	for (AVCaptureDevice *device in devices)
	{
		if ([device position] == position)
		{
			captureDevice = device;
			break;
		}
	}
	
	return captureDevice;
}

#pragma mark UI

- (void)runStillImageCaptureAnimation
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[[[self previewView] layer] setOpacity:0.0];
		[UIView animateWithDuration:.25 animations:^{
			[[[self previewView] layer] setOpacity:1.0];
		}];
	});
}

- (void)checkDeviceAuthorizationStatus
{
	NSString *mediaType = AVMediaTypeVideo;
	
	[AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
		if (granted)
		{
			//Granted access to mediaType
			[self setDeviceAuthorized:YES];
		}
		else
		{
			//Not granted access to mediaType
			dispatch_async(dispatch_get_main_queue(), ^{
				[[[UIAlertView alloc] initWithTitle:@"AVCam!"
											message:@"AVCam doesn't have permission to use Camera, please change privacy settings"
										   delegate:self
								  cancelButtonTitle:@"OK"
								  otherButtonTitles:nil] show];
				[self setDeviceAuthorized:NO];
			});
		}
	}];
}

- (void)moviePlaybackComplete:(NSNotification *)notification {
    MPMoviePlayerController *moviePlayerController = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayerController];
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.playercontroller.view.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         [self.playercontroller.view removeFromSuperview];
                     }];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:[UIApplication sharedApplication]];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillEnterForegroundNotification
                                                  object:[UIApplication sharedApplication]];
}

- (void)clearDocumentsDirectory {
    NSFileManager *fManager = [NSFileManager defaultManager];
    NSString *item;
    NSArray *contents = [fManager contentsOfDirectoryAtPath:[NSString documentsDirectory] error:nil];
    
    // >>> this section here adds all files with the chosen extension to an array
    for (item in contents){
        NSString *filePath = [[NSString documentsDirectory] stringByAppendingPathComponent:item];
        [fManager removeItemAtPath:filePath error:nil];
    }
}

//
- (void)hideExportViewsAndShowRecordButton {
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.5
                         animations:^{
                             [UIView animateWithDuration:0.5 animations:^{
                                 [self.progressView hideExportViews];
                                 self.recordButton.alpha = 1.0f;
                                 self.progressView.exportProgressBar.progress = 0.0f;
                                 self.progressView.exportProgressLabel.text = [NSString stringWithFormat:@"Processing video..."];
                                 self.progressView.exportOriginalVideoSizeLabel.text = @"calculating original size...";
                                 self.progressView.exportNewVideoSizeLabel.text      = @"calculating new video size...";
                             }];
                         }
                         completion:^(BOOL finished) {
                             [self.progressView.processActivityIndicator stopAnimating];
                         }];
    });
}

@end
























