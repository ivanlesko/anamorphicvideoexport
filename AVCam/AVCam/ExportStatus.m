//
//  ExportStatus.m
//  AVCam
//
//  Created by Ivan Lesko on 5/8/14.
//  Copyright (c) 2014 Apple Inc. All rights reserved.
//

#import "ExportStatus.h"

#import "Constants.h"

@implementation ExportStatus

+ (ExportStatus *)sharedInstance {
    static ExportStatus *sharedExportStatus = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedExportStatus = [[self alloc] init];
        sharedExportStatus.isExporting = (BOOL)[[NSUserDefaults standardUserDefaults] objectForKey:kAVIsExporting];
    });
    
    return sharedExportStatus;
}

@end
