//
//  NSString+Directories.h
//  AVCam
//
//  Created by Ivan Lesko on 5/6/14.
//  Copyright (c) 2014 Apple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Directories)

+ (NSString *)documentsDirectory;

+ (NSString *)fileOutputNameForCurrentTime;

@end
