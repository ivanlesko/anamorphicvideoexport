//
//  ProgressView.m
//  AVCam
//
//  Created by Ivan Lesko on 5/8/14.
//  Copyright (c) 2014 Apple Inc. All rights reserved.
//

#import "ProgressView.h"

@implementation ProgressView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)showExportViews {
    // Update the UI elements on the main thread, since this application relies a lot on multi-threading
    dispatch_async(dispatch_get_main_queue(), ^{
        self.processActivityIndicator.alpha = 1.0f;
        self.exportProgressBar.alpha = 1.0f;
        self.exportProgressLabel.alpha = 1.0f;
        self.exportBackgroundView.alpha = 0.5f;
        self.exportOriginalVideoSizeLabel.alpha = 1.0f;
        self.exportNewVideoSizeLabel.alpha = 1.0f;
    });
}

- (void)hideExportViews {
    // Update the UI elements on the main thread, since this application relies a lot on multi-threading
    dispatch_async(dispatch_get_main_queue(), ^{
        self.processActivityIndicator.alpha = 0.0f;
        self.exportProgressBar.alpha = 0.0f;
        self.exportProgressLabel.alpha = 0.0f;
        self.exportBackgroundView.alpha = 0.0f;
        self.exportOriginalVideoSizeLabel.alpha = 0.0f;
        self.exportNewVideoSizeLabel.alpha = 0.0f;
    });
}

@end
