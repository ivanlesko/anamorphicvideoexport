//
//  ExportStatus.h
//  AVCam
//
//  Created by Ivan Lesko on 5/8/14.
//  Copyright (c) 2014 Apple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExportStatus : NSObject

@property (nonatomic) BOOL isExporting;

+ (ExportStatus *)sharedInstance;

@end
