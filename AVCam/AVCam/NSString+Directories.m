//
//  NSString+Directories.m
//  AVCam
//
//  Created by Ivan Lesko on 5/6/14.
//  Copyright (c) 2014 Apple Inc. All rights reserved.
//

#import "NSString+Directories.h"

@implementation NSString (Directories)

+ (NSString *)documentsDirectory {
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    return path.firstObject;
}

+ (NSString *)fileOutputNameForCurrentTime
{
    // Create a movie name based on the current date and time.
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeStyle = NSDateFormatterMediumStyle;
    
    NSCharacterSet *charactersToRemove = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    
    NSString *dateString = [formatter stringFromDate:date];
    dateString = [[dateString componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    return dateString;
}

@end
