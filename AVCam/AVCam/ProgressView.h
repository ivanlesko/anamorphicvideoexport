//
//  ProgressView.h
//  AVCam
//
//  Created by Ivan Lesko on 5/8/14.
//  Copyright (c) 2014 Apple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 * ProgressView displays after the user finishes recording a video.
 * It displays progress information about the video desqueeze export.
 */

@interface ProgressView : UIView

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *processActivityIndicator;
@property (weak, nonatomic) IBOutlet UIProgressView *exportProgressBar;
@property (weak, nonatomic) IBOutlet UILabel *exportProgressLabel;
@property (weak, nonatomic) IBOutlet UIView *exportBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *exportOriginalVideoSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *exportNewVideoSizeLabel;

- (void)showExportViews;
- (void)hideExportViews;

@end
